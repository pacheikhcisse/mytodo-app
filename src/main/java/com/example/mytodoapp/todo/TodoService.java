package com.example.mytodoapp.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class TodoService {

    @Autowired
    private TodoDao todoDao;

    public List<Todo> getTodoList() {
        return todoDao.list();
    }

    public void addTodo(Todo todo) {
        todoDao.save(todo);
    }

    public void removeTodo(UUID id) {
        todoDao.remove(id);
    }

}