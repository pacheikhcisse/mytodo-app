package com.example.mytodoapp;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "json:target/cucumber/report.json", "html:target/cucumber/report.html"},
        features = "src/test/resources/features"
)
public class MytodoAppIT {
}
