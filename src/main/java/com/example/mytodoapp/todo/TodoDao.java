package com.example.mytodoapp.todo;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Component
public class TodoDao {

    private List<Todo> storage = new ArrayList<>();

    public List<Todo> list() {
        return Collections.unmodifiableList(storage);
    }

    public void save(Todo todo) {
        storage.add(todo);
    }

    public void remove(UUID id) {
        storage.removeIf(todo -> todo.getId().equals(id));
    }

}