package com.example.mytodoapp.todo;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.List;
import java.util.stream.Collectors;

public class AddTodo {

    @LocalServerPort
    private int port;

    private static WebDriver driver;

    @Given("I am on the todolist page")
    public void go_to_todolist_page() {
        System.setProperty("webdriver.chrome.driver", "C:\\seleniumdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://localhost:" + port + "/todolist");
    }

    @When("I fill {string} form field with {string}")
    public void fill_form_field(String formField, String text) {
        driver.findElement(By.name(formField.toLowerCase()))
                .sendKeys(text);
    }

    @And("I click in Add button")
    public void click_add_button() {
        driver.findElement(By.xpath("//button[@type='submit' and text()='Add']"))
                .click();
    }

    @Then("I should have {string} in the todo list")
    public void todo_should_be_added(String expectedTodoName) throws InterruptedException {
        List<WebElement> elementList = driver.findElements(By.className("todo"));
        List<String> todoList = elementList.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());

        Thread.sleep(5000);
        driver.quit();

        Assert.assertTrue(todoList.contains(expectedTodoName));
    }

}
