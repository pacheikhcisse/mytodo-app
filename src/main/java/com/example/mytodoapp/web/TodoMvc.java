package com.example.mytodoapp.web;

import com.example.mytodoapp.todo.Todo;
import com.example.mytodoapp.todo.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

@Controller
public class TodoMvc {

    @Autowired
    private TodoService todoService;

    @GetMapping("/todolist")
    public String getTodoList(Model model) {
        model.addAttribute("todos", todoService.getTodoList());
        return "todo";
    }

    @PostMapping(value = "/todolist", headers = "content-type=application/x-www-form-urlencoded")
    public String addTodo(@RequestParam(value = "title") String title,
                          @RequestParam(value = "description") String description) {
        todoService.addTodo(new Todo(title, description));
        return "redirect:todolist";
    }

    @DeleteMapping(value = "/todolist")
    public String removeTodo(@RequestParam(value = "id") UUID id) {
        todoService.removeTodo(id);
        return "redirect:todolist";
    }

}
