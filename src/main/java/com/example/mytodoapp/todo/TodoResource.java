package com.example.mytodoapp.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/todo")
public class TodoResource {

    @Autowired
    private TodoService todoService;

    @GetMapping
    public List<Todo> getTodoList() {
        return todoService.getTodoList();
    }

    @PostMapping
    public void addTodo(@RequestBody Todo todo) {
        todoService.addTodo(todo);
    }

    @DeleteMapping(value = "/{id}")
    public void remove(@PathVariable(value = "id") String id) {
        todoService.removeTodo(UUID.fromString(id));
    }

}
