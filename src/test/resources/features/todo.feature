Feature: Todo

  Scenario: As a user, I want to add a todo
    Given I am on the todolist page
    When I fill "Title" form field with "My todo"
    And I fill "Description" form field with "My todo description"
    And I click in Add button
    Then I should have "My todo" in the todo list